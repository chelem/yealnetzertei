import argparse

from db import connect_to_exist, ensure_collection
import settings

db = None


def get_arguments(args=None):
    """ Parse the command line arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument('--overwrite', default=False, action='store_true', help="Convert documents that have already been converted")
    return parser.parse_args(args)


def raw_path_to_tei(raw_path):
    """ Take a path in the raw collection and convert it to the corresponding path in the TEI collection"""
    if not raw_path.startswith(settings.RAW_COLLECTION):
        raise ValueError("%s is not a Raw Collection path" % raw_path)

    tei_path = settings.TEI_COLLECTION + raw_path[len(settings.RAW_COLLECTION):]
    return tei_path


def retrieve_raw_documents():
    xquery = 'for $x in collection("/db/%s") return $x' % settings.RAW_COLLECTION
    result_id = db.executeQuery(xquery)
    summary = db.querySummary(result_id)
    print("First document name: %s" % summary['documents'][0][0])

    doc = db.retrieve_text(result_id, 0)
    print("First document: %s" % doc)


def convert_files(overwrite):
    retrieve_raw_documents()

def main():
    global db
    args = get_arguments()

    db = connect_to_exist()
    ensure_collection(db, settings.TEI_COLLECTION)
    convert_files(args.overwrite)


if __name__ == '__main__':
    main()
