import argparse
import os
import sys

from lxml import etree
from pyexistdb.exceptions import ExistDBException

from db import connect_to_exist, ensure_collection, does_path_exist
import settings

db = None

def get_arguments(args=None):
    """ Parse the command line arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', type=str, help="Directory of files to import")
    parser.add_argument('--overwrite', default=False, action='store_true', help="Overwrite documents in the database")
    return parser.parse_args(args)


def path_to_dbpath(path, toppath):
    """ Converts a filename with is under toppath to a path in the database
    We remote toppath from the path, drop the file extension (.xml usually) and replace backslahses with slashes.
    We then prepend our base collection and /raw/
    """
    if path[:len(toppath)] != toppath:
        raise ValueError("Path %s does not start with toppath %s" % (path, toppath))
    path = path[len(toppath):]

    path = os.path.splitext(path)[0]  # Drop the extension
    path = path.replace("\\", "/")    # Replace Windows's backslashes
    path = settings.RAW_COLLECTION + path
    return path


# Keep track of number of imported and skipped files.
imported = 0
skipped = 0


def import_file(file_path, db_path, overwrite):
    global imported, skipped

    extension = os.path.splitext(file_path)[1]
    if extension.lower() != '.xml':
        skipped += 1
        return

    if not overwrite and does_path_exist(db, db_path):
        skipped += 1
        print("%06d %s skipped" % (imported + skipped, file_path))
        return

    try:
        with open(file_path, 'rb') as input:
            db.load(input, db_path)
        imported += 1
        print("%06d %s" % (imported + skipped, file_path))
    except Exception as e:
        print("Can't save XML from file %s to database: %s" % (file_path, str(e)))


def count_files(top_dir):
    count = 0
    for (_, _, filenames) in os.walk(top_dir):
        count += len(filenames)
    return count


def import_files(top_dir, overwrite):
    count = count_files(top_dir)
    print("About to import %d files" % count)

    for (dirpath, dirnames, filenames) in os.walk(top_dir):
        for filename in filenames:
            file_path = os.path.join(dirpath, filename)
            db_path = path_to_dbpath(file_path, top_dir)
            import_file(file_path, db_path, overwrite)

    print("Done, %d files imported, %d files skipped" % (imported, skipped))


def main():
    global db
    args = get_arguments()

    if not os.path.isdir(args.directory):
        print("Directory " + args.directory + " does not exist", file=sys.stderr)
        return
    db = connect_to_exist()
    ensure_collection(db, settings.RAW_COLLECTION)
    import_files(args.directory, args.overwrite)


if __name__ == '__main__':
    main()
