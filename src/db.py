from pyexistdb.db import ExistDB
from pyexistdb.exceptions import ExistDBException

import settings


def connect_to_exist():
    """ Connects to ExistDB using the credentials in settings.py """
    return ExistDB(settings.EXISTDB_SERVER_URL,
                   username=settings.EXISTDB_SERVER_USER,
                   password=settings.EXISTDB_SERVER_PASSWORD)


def ensure_collection(db, collection):
    """ Makes sure a collection exists - creates it if it doesn't """
    if not db.hasCollection(collection):
        db.createCollection(collection)

def does_path_exist(db, db_path):
    """ Checks whether a document exists at db-path """
    try:
        db.getDocument(db_path)
        return True
    except ExistDBException:
        return False
